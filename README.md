# Tic Tac Toe Student

A framework for students to build a simple tic tac toe game in Java.
The **Top Down** design is done, you just need to complete the **Bottom
Up** implementation. 

This is a simple framework of partially developed classes to allow 
students to create their own tic tac toe game.

## Classes

### BoardInterface

This is an interface to describe how the `Board` class will act.

### Board

`Board` is the class that will let you describe the data and actions
that the game will need.

### Game

A driver class to let you build the `.play()` method where the game 
will take place. This is where you use the board 

### TicTacToe

The class that you will run. The class tha has the `main` method.

