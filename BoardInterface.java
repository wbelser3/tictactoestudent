/**
 * BoardInterface defines the requirements for your Board class.  The board
 * is a two D array [3][3] that has positions 1-9:
 *
 * <pre>
 * 1 2 3
 * 4 5 6
 * 7 8 9
 * </pre>
 *
 * the player is X and the computer is O
 *
 * @author wbelser
 * @version 152
 */
public interface BoardInterface {

    /**
     * A constant to set a grid position to empty
     */
    public static final int EMPTY = 0;

    /**
     * A constant to set a grid position to player X
     */
    public static final int X = 1;

    /**
     * A constant to set a grid position to player O
     */
    public static final int O = 2;

    /**
     * initBoard will initialize the board as all EMPTY.
     */
    void initBoard();


    /**
     * toString will return a String representing the board and all of
     * the occupied positions.
     *
     * @return a String will be returned representing the board and all of
     * the occupied positions
     */
    String toString();

    /**
     * getOpenPositions lets me know what moves are open. Example:
     * <pre>
     *     1, 2, 4, 5, 6, 7, 8
     * </pre>
     *
     * @return a comma delimited String of all the open moves
     */
    String getOpenPositions();

    /**
     * hasMoves will let you know if there is an open move on the board.
     *
     * @return boolean indicating if there is an open move.
     */
    boolean hasMoves();

    /**
     * placePlayer will try to put any player in the position indicated.
     *
     * @param player int indicating what player is being placed on the board
     * @param position int 1 to 9 for the position on the board
     * @return a boolean indicating if the placement was successful
     */
    boolean placePlayer(int player, int position);

    /**
     * placeX will try to put an X in the position indicated.
     *
     * @param position int 1 to 9 for the position on the board
     * @return a boolean indicating if the placement was successful
     */
    boolean placeX(int position);

    /**
     * placeO will try and place O in an open position.
     *
     * @return a boolean indicating if there was a successful placement of the opponent
     */
    boolean placeO();

    /**
     * checkForWinner will see it anyone has won the game.
     *
     * @return returns an int of the winner if any
     */
    int checkForWinner();

}
