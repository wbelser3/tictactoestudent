public class Board implements BoardInterface {

    /**
     * A 2D int array to hold the constant representing who is in the
     * current position on the grid
     */
    private int[][] grid;

    /**
     * Create a board object, build the 3x3 grid, and initialize the board
     * to EMPTY
     */
    public Board(){
        grid = new int[3][3];
        initBoard();
    }

    // Public methods need by the Game driver class

    public void initBoard(){
    }

    public String toString() {
        return "";
    }

    public String getOpenPositions() {
        return "1, 2, 9";
    }

    public boolean hasMoves() {
        return false;
    }

    public boolean placePlayer(int player, int position) {
        return false;
    }

    public boolean placeX(int position) {
        return false;
    }

    public boolean placeO() {
        return false;
    }

    public int checkForWinner() {
        return Board.EMPTY;
    }

    // Private methods that help with the class

    /**
     * rowFromPosition will tell me which row this position should be.
     *
     * @param position pass it position 1-9
     * @return the correct row for that position
     */
    private int rowFromPosition(int position) {
        return -1;
    }

    /**
     * colFromPosition will tell me which row this position should be.
     *
     * @param position pass it position 1-9
     * @return the correct column for that position
     */
    private int colFromPosition(int position) {
        return -1;
    }

    /**
     * isEmpty will let you know if a position is empty or not. If it is
     * empty, return true otherwise return false.
     *
     * @param position int 1 to 9 for the position on the board
     * @return a boolean indicating if the position is empty
     */
    private boolean isEmpty(int position) {
        return false;
    }

    /**
     * whoIsAt will tell you who is at the position
     *
     * @param position int 1 to 9 for the position on the board
     * @return returns an int of the player in that position
     */
    private int whoIsAt(int position) {
        return Board.EMPTY;
    }

}
