import java.util.Scanner;

/**
 * Game is the driver class for Tic Tac Toe. It has just one public
 * method play. Use the keyboard scanner to get an int from the player.
 *
 * PRECONDITION: assume the player will only enter an int
 *
 */
public class Game {

    /**
     * play method is the driver method where you will build the
     * the working of the tic tac toe game
     */
    public void play() {

        // need a board to play
        Board board = new Board();

        // Need to be able to get an int for position
        Scanner keyboard = new Scanner(System.in);

        // get a move
        System.out.println("");
        System.out.print("Available moves are : " + board.getOpenPositions() + " : ");
        int move = keyboard.nextInt();
        System.out.println("move = " + move);

    }

}
